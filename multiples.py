"""
Instructions:
Write a program that prints all the numbers from 1 to 100. However,
for multiples of 3, instead of the number, print "Foo".
For multiples of 5 print "Bar". For numbers which are
multiples of both 3 and 5, print "FooBar".

But here's the catch: you can use only one if.
No multiple branches, ternary operators or else.

Requirements:
  1 if
  You can't use else, else if or ternary
  Unit tests
  Feel free to apply your S.O.L.I.D knowledge (highly appreciated)
  You can write the challenge in any language you want.
"""


def get_multiples(start, stop):
    """
    Get multiples.
    """
    return [
        (n % 3 == 0 and n % 5 == 0 and 'FooBar') or
        (n % 3 == 0 and n != 3 and 'Foo') or
        (n % 5 == 0 and n != 5 and 'Bar') or
        (n) for n in range(start, stop+1)
    ]
