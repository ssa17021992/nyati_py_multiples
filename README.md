# Nyati py multiples

Instructions:
Write a program that prints all the numbers from 1 to 100. However,
for multiples of 3, instead of the number, print "Foo".
For multiples of 5 print "Bar". For numbers which are
multiples of both 3 and 5, print "FooBar".

But here's the catch: you can use only one if.
No multiple branches, ternary operators or else.

Requirements:
  1 if
  You can't use else, else if or ternary
  Unit tests
  Feel free to apply your S.O.L.I.D knowledge (highly appreciated)
  You

### Running program

```bash
(venv)$ python3 init.py
```

## Running tests

Explain how to run the automated tests for this system

```bash
(venv)$ python3 tests.py
```

## Contributing

Please read CONTRIBUTING file for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

See the list of contributors who participated in this project.

## License

This project is licensed under... - see the LICENSE.md file for details.
