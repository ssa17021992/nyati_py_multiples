import unittest

from multiples import get_multiples


class MultiplesTestCase(unittest.TestCase):
    """
    Multiples test.
    """

    def setUp(self):
        self.multiples = get_multiples(1, 100)

    def tearDown(self):
        self.multiples = None

    def test_multiples_of_three(self):
        self.assertEqual(self.multiples[5], 'Foo')  # 6 is multiple of 3?
        self.assertEqual(self.multiples[8], 'Foo')  # 9 is multiple of 3?
        self.assertNotEqual(self.multiples[2], 'Foo')  # 3 is not multiple of 3?

    def test_multiples_of_five(self):
        self.assertEqual(self.multiples[9], 'Bar')  # 10 is multiple of 5?
        self.assertEqual(self.multiples[19], 'Bar')  # 20 is multiple of 5?
        self.assertNotEqual(self.multiples[4], 'Bar')  # 5 is not multiple of 5?

    def test_multiples_both_three_and_five(self):
        self.assertEqual(self.multiples[14], 'FooBar')  # 15 is multiple of 3 and 5?
        self.assertEqual(self.multiples[29], 'FooBar')  # 30 is multiple of 3 and 5?
        self.assertNotEqual(self.multiples[2], 'FooBar')  # 3 is not multiple of 3 and 5?
        self.assertNotEqual(self.multiples[4], 'FooBar')  # 5 is not multiple of 3 and 5?


if __name__ == '__main__':
    unittest.main()
